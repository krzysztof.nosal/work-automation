﻿#SingleInstance Force
#NoTrayIcon
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


#Include HotStrings.ahk

hotstrings("i)(\w+)\+\+", "%$1% := %$1%+1; ") 
hotstrings("i)(\w+)\+\=(\d+)\;", "%$1% := %$1%+%$2%; ") 

!F2::ExitApp